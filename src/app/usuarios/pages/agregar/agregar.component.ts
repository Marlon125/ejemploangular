import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Usuario } from '../../../models/usuario';
import { UsuariosService } from '../../services/usuarios.service';
import { ConfirmarComponent } from '../components/confirmar/confirmar.component';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styles: [
  ]
})
export class AgregarComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);

  usuario: Usuario = {
    nombre: '',
    apellido: '',
    email: '',
    estado: true
  }

  constructor(
    private usuariosService: UsuariosService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog ) { }

  ngOnInit(): void {
    this.activatedRoute.params
    .pipe(
      switchMap(({id}) => this.usuariosService.getUsuarioPorId(id))
    )
    .subscribe(
      usuario => this.usuario = usuario
    )
  }

  guardar(){
    if(this.usuario.id){
      this.usuariosService.actualizarUsuario(this.usuario)
      .subscribe(usuario => this.mostrarSnakbar('Registro actualizado'))
    }else{
      this.usuariosService.agregarUsuario(this.usuario)
      .subscribe(usuario => {
        this.router.navigate(['/usuarios/editar', usuario.id])
        this.mostrarSnakbar('Registro creado')
      })
    }
  }

  borrarUsuario(){
    const dialog = this.dialog.open( ConfirmarComponent, {
      width: '250px',
      data: this.usuario
    });

    dialog.afterClosed().subscribe(
      (result) => {
        if(result){
          this.usuariosService.eliminarUsuario(this.usuario)
          .subscribe(resp => {
            this.router.navigate(['usuarios'])
          })
        }
      }
    )
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  mostrarSnakbar( mensaje: string ) {

    this.snackBar.open( mensaje, 'ok!', {
      duration: 2500
    });

  }

}
