import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgregarComponent } from './pages/agregar/agregar.component';
import { DetalleComponent } from './pages/detalle/detalle.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';
import { ConfirmarComponent } from './pages/components/confirmar/confirmar.component';
import { EstadoPipe } from '../pipes/estado.pipe';
import { BuscarComponent } from './pages/buscar/buscar.component';



@NgModule({
  declarations: [
    AgregarComponent,
    DetalleComponent,
    ListadoComponent,
    HomeComponent,
    ConfirmarComponent,
    EstadoPipe,
    BuscarComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    UsuariosRoutingModule,
    MaterialModule
  ]
})
export class UsuariosModule { }
