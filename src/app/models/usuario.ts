export interface Usuarios {
    usuarios: Usuario[];
}

export interface Usuario {
    id?:       number;
    nombre:   string;
    apellido?: string;
    email:    string;
    estado:   boolean;
    createdAt?: Date;
    updatedAt?: Date;
}