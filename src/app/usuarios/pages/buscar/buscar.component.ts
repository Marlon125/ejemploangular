import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styles: [
  ]
})
export class BuscarComponent implements OnInit {

  termino: string  = '';
  heroes: Usuario[] = [];
  heroeSeleccionado: Usuario | undefined;

  constructor(private usuarioService: UsuariosService) { }

  ngOnInit(): void {
  }

  buscando(){
    // this.usuarioService.
  }

}
