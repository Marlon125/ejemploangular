import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styles: [
    `table {
      width: 95%;
    }`
  ]
})

export class ListadoComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nombre', 'apellido', 'email', 'estado', 'createdAt', 'updatedAt'];
  dataSource: Usuario[] = [];
  clickedRows = new Set<Usuario>();


  constructor(private usuarioService: UsuariosService) { }

  ngOnInit(): void {
    this.usuarioService.getUsuarios().subscribe(data =>{
      this.dataSource = data;
      console.log(data)
    } )
  }

}
