import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Usuario, Usuarios } from 'src/app/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  // definimos la url de donde se va a apuntar al servicio web
  private baseUrl: string = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.baseUrl}/usuarios`);
  }

  getUsuarioPorId(id: string): Observable<Usuario>{
    return this.http.get<Usuario>(`${this.baseUrl}/usuarios/${id}`);
  }

  agregarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(`${this.baseUrl}/usuarios`, usuario);
  }

  actualizarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(`${this.baseUrl}/usuarios/${usuario.id}`, usuario);
  }

  eliminarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.delete<Usuario>(`${this.baseUrl}/usuarios/${usuario.id}`);
  }


}
